(() => {

  'use strict';

  /**************** Gulp.js 4 configuration ****************/

  const

      // development or production
      devBuild = ((process.env.NODE_ENV || 'development').trim().
          toLowerCase() === 'development'),

      // directory locations
      dir = {
        src: 'src/',
        build: 'build/',
      },

      // modules
      gulp = require('gulp'),
      del = require('del'),
      noop = require('gulp-noop'),
      newer = require('gulp-newer'),
      size = require('gulp-size'),
      imagemin = require('gulp-imagemin'),
      sass = require('gulp-sass'),
      postcss = require('gulp-postcss'),
      lazypipe = require('lazypipe'),
      shtml = require('gulp-shtml'),
      header = require('gulp-header'),
      jshint = require('gulp-jshint'),
      stylish = require('jshint-stylish'),
      concat = require('gulp-concat'),
      uglify = require('gulp-uglify'),
      tap = require('gulp-tap'),
      optimizejs = require('gulp-optimize-js'),
      _package = require('./package.json'),
      rename = require('gulp-rename'),
      plumber = require('gulp-plumber'),
      sourcemaps = devBuild ? require('gulp-sourcemaps') : null,
      browsersync = devBuild ? require('browser-sync').create() : null;

  console.log('Gulp', devBuild ? 'development' : 'production', 'build');

  /**
   * Template for banner to add to file headers
   */

  var banner = {
    full:
    '/*!\n' +
    ' * <%= package.name %> v<%= package.version %>: <%= package.description %>\n' +
    ' * (c) ' + new Date().getFullYear() + ' <%= package.author.name %>\n' +
    ' * <%= package.license %> License\n' +
    ' * <%= package.repository.url %>\n' +
    ' */\n\n',
    min:
    '/*!' +
    ' <%= package.name %> v<%= package.version %>' +
    ' | (c) ' + new Date().getFullYear() + ' <%= package.author.name %>' +
    ' | <%= package.license %> License' +
    ' | <%= package.repository.url %>' +
    ' */\n',
  };

  /**************** clean task ****************/

  function clean() {

    return del([dir.build]);

  }

  exports.clean = clean;
  exports.wipe = clean;

  /**************** images task ****************/

  const imgConfig = {
    src: dir.src + 'images/**/*',
    build: dir.build + 'images/',

    minOpts: {
      optimizationLevel: 5,
    },
  };

  function images() {

    return gulp.src(imgConfig.src).
        pipe(newer(imgConfig.build)).
        pipe(imagemin(imgConfig.minOpts)).
        pipe(size({showFiles: true})).
        pipe(gulp.dest(imgConfig.build));

  }

  exports.images = images;

  /**************** CSS task ****************/

  const cssConfig = {

    src: dir.src + 'scss/main.scss',
    watch: dir.src + 'scss/**/*',
    build: dir.build + 'css/',
    sassOpts: {
      sourceMap: devBuild,
      outputStyle: 'nested',
      imagePath: '/images/',
      precision: 3,
      errLogToConsole: true,
    },

    postCSS: [
      require('postcss-assets')({
        loadPaths: ['images/'],
        basePath: dir.build,
      }),
      require('autoprefixer')({
        browsers: ['> 1%'],
      }),
    ],

  };

  // remove unused selectors and minify production CSS
  if (!devBuild) {
    console.log('Dev run');
    cssConfig.postCSS.push(
        require('postcss-uncss')({
          html: ['index.html'],
          ignore: ['.fade'],
        }),
        require('cssnano'),
    );

  }

  function css() {

    return gulp.src(cssConfig.src).
        pipe(sourcemaps ? sourcemaps.init() : noop()).
        pipe(sass(cssConfig.sassOpts).on('error', sass.logError)).
        pipe(postcss(cssConfig.postCSS)).
        pipe(sourcemaps ? sourcemaps.write() : noop()).
        pipe(size({showFiles: true})).
        pipe(gulp.dest(cssConfig.build)).
        pipe(browsersync ? browsersync.reload({stream: true}) : noop());

  }

  exports.css = gulp.series(images, css);

  /**************** HTML task ****************/
  const HTMLConfig = {
    src: dir.src,
    watch: dir.src + '**/**',
    build: dir.build,
  };

  function html() {
    return gulp.src([HTMLConfig.src + '/*.*html', '!/includes/*.*html'],
        {base: HTMLConfig.src}).
        pipe(shtml({
          wwwroot: HTMLConfig.src,
        })).
        pipe(gulp.dest(HTMLConfig.build)).
        pipe(browsersync ? browsersync.reload({stream: true}) : noop());
  }

  exports.html = gulp.series(clean, html);

  /**************** Javascript task ****************/

  const jsConfig = {
    src: dir.src + 'js/**',
    watch: dir.src + 'js/**',
    build: dir.build + 'js',
    polyfills: '!' + dir.src + 'js/*.polyfill.js',
  };

  const jsTasks = lazypipe().
      pipe(header, banner.full, {package: _package}).
      pipe(optimizejs).
      pipe(gulp.dest, jsConfig.build).
      pipe(rename, {suffix: '.min'}).
      pipe(uglify).
      pipe(optimizejs).
      pipe(header, banner.min, {package: _package}).
      pipe(gulp.dest, jsConfig.build);

  function javascript() {

    // Lint, minify, and concatenate scripts

    return gulp.src([jsConfig.src, jsConfig.polyfills]).
        pipe(plumber()).
        pipe(jsTasks()).
        pipe(browsersync ? browsersync.reload({stream: true}) : noop());
  }

  function javascript_with_polyfill() {
    // Create scripts with polyfills

    return gulp.src(jsConfig.src).
        pipe(plumber()).
        pipe(concat(_package.name + '.js')).
        pipe(rename({
          suffix: '.polyfills',
        })).
        pipe(jsTasks()).
        pipe(browsersync ? browsersync.reload({stream: true}) : noop());
  }

  function javascripts_hint() {
    return gulp.src(jsConfig.src).
        pipe(plumber()).
        pipe(jshint()).
        pipe(jshint.reporter('jshint-stylish')).
        pipe(browsersync ? browsersync.reload({stream: true}) : noop());
  }

  exports.javascript = gulp.series(
      javascript,
      javascript_with_polyfill,
      javascripts_hint,
  );

  /**************** server task (now private) ****************/

  const syncConfig = {
    server: {
      baseDir: './build',
      index: 'index.html',
    },
    port: 8000,
    open: false,
  };

  // browser-sync
  function server(done) {
    if (browsersync) browsersync.init(syncConfig);
    done();
  }

  /**************** watch task ****************/

  function watch(done) {

    // image changes
    gulp.watch(imgConfig.src, images);

    // CSS changes
    gulp.watch(cssConfig.watch, css);
    gulp.watch(HTMLConfig.watch, html);
    gulp.watch(jsConfig.watch, javascript);
    gulp.watch(jsConfig.watch, javascript_with_polyfill);
    gulp.watch(jsConfig.watch, javascripts_hint);

    done();

  }

  /**************** default task ****************/

  exports.default = gulp.series(clean, exports.css, javascript,
      javascript_with_polyfill, javascripts_hint, html, watch, server);

})();
